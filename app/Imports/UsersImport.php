<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Log;

class UsersImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
       

        // Valida que los datos requeridos no sean nulos
        if (isset($row['name']) && isset($row['lastname']) && isset($row['password'])) {
            return new User([
                'name' => $row['name'],
                'lastname' => $row['lastname'],
                'affiliate_number' => $row['affiliate_number'],
                'dni' => $row['dni'],
                'celphone' => $row['celphone'],
                'email' => $row['email'],
                'date_from' => $this->transformDate($row['date_from']),
                'date_until' => $this->transformDate($row['date_until']),
                'active' => $row['active'],
                'password' => Hash::make($row['password']),
            ]);
        } else {
            Log::error('Faltan datos requeridos en la fila importada', $row);
            return null;
        }
    }

    private function transformDate($value)
    {
        if (empty($value)) {
            return null;
        }
        
        // Asume que las fechas en Excel están en formato numérico y las convierte a 'Y-m-d'
        return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value))->format('Y-m-d');
    }
}
