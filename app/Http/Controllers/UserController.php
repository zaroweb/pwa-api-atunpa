<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    public function showUploadForm()
    {
        return view('upload');
    }

    public function upload(Request $request)
    {
        // Validar si se ha enviado un archivo
        if ($request->hasFile('import_file')) {
            $file = $request->file('import_file');

            // Definir la ruta donde se guardará el archivo
            $destinationPath = storage_path('app/uploads');
            $fileName = $file->getClientOriginalName();

            // Mover el archivo al directorio deseado
            $file->move($destinationPath, $fileName);

            $filePath = $destinationPath . '/' . $fileName;

            Log::info('Ruta del archivo guardado', ['filePath' => $filePath]);

            try {
                // Importar los datos del archivo usando el paquete Excel
                Excel::import(new UsersImport, $filePath);

                // Eliminar el archivo después de importarlo si es necesario
                // unlink($filePath); // Descomenta esto si deseas eliminar el archivo después de importarlo

                return back()->with('success', 'Usuarios importados con éxito.');
            } catch (\Exception $e) {
                Log::error('Error al importar usuarios desde Excel', ['exception' => $e]);
                return back()->with('error', 'Error al importar usuarios. Por favor, verifica el formato del archivo.');
            }
        } else {
            Log::error('No se encontró ningún archivo en la solicitud');
            return back()->with('error', 'No se encontró ningún archivo.');
        }
    }

    public function register(Request $request)
    {
        // Validar la solicitud
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'affiliate_number' => 'required|string|max:255|unique:users',
            'dni' => 'required|string|max:255|unique:users',
            'celphone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        // Retornar errores de validación
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Crear el usuario
        $user = User::create([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'affiliate_number' => $request->affiliate_number,
            'dni' => $request->dni,
            'celphone' => $request->celphone,
            'date_from' => $request->date_from,
            'date_until' => $request->date_until,
            'active' => $request->active,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // Retornar la respuesta de éxito
        return response()->json(['message' => 'User created successfully', 'user' => $user], 201);
    }
    
    
    
    public function login(Request $request)
    {
        // Validar la solicitud
        $validator = Validator::make($request->all(), [
            'dni' => 'required|string',
            'password' => 'required|string',
        ]);
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    
        // Buscar al usuario por DNI
        $user = User::where('dni', $request->dni)->first();
    
        // Verificar si el usuario existe y la contraseña es correcta
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(['message' => 'DNI o contraseña inválida'], 401);
        }
    
        // Verificar si la fecha actual es posterior a date_until
        $currentDate = now();  // Obtener la fecha y hora actual
        $dateUntil = $user->date_until;  // Obtener la fecha hasta la cual es válido el usuario
    
        if ($currentDate->greaterThanOrEqualTo($dateUntil)) {
            return response()->json(['message' => 'Acceso denegado. Tu afiliación no está activa. Comunicate con el Sindicato.'], 401);
        }
    
        // Crear un token para el usuario
        $token = $user->createToken('auth_token')->plainTextToken;
    
        // Retornar los datos del usuario junto con el token
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'affiliate_number' => $user->affiliate_number,
                'dni' => $user->dni,
                'celphone' => $user->celphone,
                'email' => $user->email,
                'date_from' => $user->date_from,
                'date_until' => $user->date_until,
                'image_url' => $user->image_url,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
            ],
        ]);
    }
    
    public function updateImage(Request $request, $id)
    {
        // Validar la solicitud
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif', // Ajusta los tipos y el tamaño según tus necesidades
        ]);

        // Retornar errores de validación
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Buscar al usuario por ID
        $user = User::find($id);

        // Verificar si el usuario existe
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        // Guardar la imagen en el almacenamiento
        $imagePath = $request->file('image')->store('public/images');

        // Obtener la URL de la imagen
        $imageUrl = Storage::url($imagePath);

        // Actualizar la URL de la imagen en la base de datos
        $user->image_url = $imageUrl;
        $user->save();

        // Retornar la respuesta de éxito
        return response()->json(['message' => 'Image uploaded successfully', 'image_url' => $imageUrl], 200);
    }
}
